<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controllers
{
    public function index(){
        return view('create');
    }
    public function proses(Request $request){
        $data = array();
        $data['npm'] = $request->npm;
        $data['nama'] =$request->nama;
        $data['program studi'] = $request->programstudi;
        $data['no hp'] = $request->no_hp;
        $data['tempat_tanggal_lahir'] = $request->tempat_tanggal_lahir;
        $data['jenis_kelamin'] = $request->jenis_kelamin;
        $data['agama'] = $request->agama;
   
        return view('view', ['data' => $data]);
    }
}